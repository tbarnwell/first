FROM ubuntu:16.04
RUN echo "deb http://packages.ros.org/ros/ubuntu $(grep -e '^UBUNTU_CODENAME=' /etc/os-release | sed 's/^[^=]*=//g') main" > /etc/apt/sources.list.d/ros-latest.list
RUN apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
RUN apt-get update && apt-get -y install ros-kinetic-ros-base
RUN rosdep init
RUN rosdep update

# Set the ROS environment.
ENV ROS_ROOT /opt/ros/kinetic/share/ros
ENV ROS_PACKAGE_PATH /opt/ros/kinetic/share
ENV ROS_MASTER_UTI "http://localhost:11311"
ENV ROS_VERSION 1
ENV LD_LIBRARY_PATH /opt/ros/kinetic/lib
ENV PATH /opt/ros/kinetic/bin:$PATH
ENV ROSLISP_PACKAGE_DIRECTORIES ""
ENV ROS_DISTRO kinetic
ENV PYTHONPATH /opt/ros/kinetic/lib/python2.7/dist-packages
ENV PKG_CONFIG_PATH /opt/ros/kinetic
ENV CMAKE_PREFIX_PATH /opt/ros/kinetic
ENV ROS_ETC_DIR /opt/ros/kinetic/etc/ros

RUN env
RUN apt-cache search ros-kinetic
RUN ls -A

