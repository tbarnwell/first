#!/bin/sh

main() {
  local build_folder="$1"
  find "$build_folder" -name test_results -print0 |
  xargs -0 ./linkup_test.sh
}

main "$@"
