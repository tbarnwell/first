
#include "ros/ros.h"
#include "pthread.h"

static pthread_t last_id = 0;

void callback(const ros::TimerEvent&) {
  pthread_t tid = pthread_self();
  if (tid != last_id) {
    ROS_INFO("New TID: (%ld)", tid);
  } else {
  }
  last_id = tid;
}

int main(int argc, char ** argv) {

  ros::init(argc, argv, "timertest");

  ros::NodeHandle node("timer");

  ros::Timer timer = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer1 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer2 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer3 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer4 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer5 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer6 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer7 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer8 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer9 = node.createTimer(ros::Duration(0), callback);
  ros::Timer timer10 = node.createTimer(ros::Duration(0), callback);

  ros::spin();
  return 0;
}
