#!/bin/sh

run() {
  printf '%s\n' "$*"
  "$@"
}

translate_name() {
  local name="$1"
  printf '%s' "$name" |
  sed -e 's/test_results/test-results/'
}

main() {
  for path in "$@"; do
    local folder="$(basename "$path")"
    local path_translated="$(translate_name "$path")"
    local folder_translated="$(basename "$path_translated")"
    run mv -n "$path" "$path_translated"
    run ln -s "$folder_translated" "$path"
  done
}

main "$@"
