#!/bin/sh

main() {
  catkin build --make-args test
  local exit="$?"
  ./linkup_tests.sh build
  return "$exit"
}

main "$@"
